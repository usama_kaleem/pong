﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PedalMovement: MonoBehaviour, IEventSystemHandler {
	public GameObject player1;
	public GameObject player2;
	float speed = 10f;
	float maxspeed = 100f;
	private float original_speed = 10f;
	public static bool buttonHeldLeftP1 = false;
	public static bool buttonHeldRightP1 = false;
	public static bool buttonHeldLeftP2 = false;
	public static bool buttonHeldRightP2 = false;

	float factor = 1.05f;

	void Start ()
	{
		buttonHeldLeftP1 = false;
		buttonHeldRightP1 = false;
		buttonHeldLeftP2 = false;
		buttonHeldRightP2 = false;
	}

	public void pressedLeftP1 (BaseEventData eventData)
	{
		buttonHeldLeftP1 = true;
	}
	public void notpressedLeftP1(BaseEventData eventData)
	{
		buttonHeldLeftP1 = false;
		speed = original_speed;
	}


	public void pressedRightP1 (BaseEventData eventData)
	{
		buttonHeldRightP1 = true;
	}
	public void notpressedRightP1(BaseEventData eventData)
	{
		buttonHeldRightP1 = false;
		speed = original_speed;
	}



	public void pressedRightP2 (BaseEventData eventData)
	{
		buttonHeldRightP2 = true;
	}
	public void notpressedRightP2(BaseEventData eventData)
	{
		buttonHeldRightP2 = false;
		speed = original_speed;
	}



	public void pressedLeftP2 (BaseEventData eventData)
	{
		buttonHeldLeftP2 = true;
	}
	public void notpressedLeftP2(BaseEventData eventData)
	{
		buttonHeldLeftP2 = false;
		speed = original_speed;
	}





	public void FixedUpdate()

	{

		if (buttonHeldLeftP1)
		{
			if (speed < maxspeed) {
				speed = speed * factor;
				player1.transform.RotateAround (new Vector3 (0, 0, 0), Vector3.forward,-1*speed * Time.deltaTime);
			} else {
				player1.transform.RotateAround (new Vector3 (0, 0, 0), Vector3.forward, -1*maxspeed * Time.deltaTime);
			}
		} 

		if (buttonHeldLeftP2)
		{
			if (speed < maxspeed) {
				speed = speed * factor;
				player2.transform.RotateAround (new Vector3 (0, 0, 0), Vector3.forward, -1*speed * Time.deltaTime);
			} else {
				player2.transform.RotateAround (new Vector3 (0, 0, 0), Vector3.forward, -1*maxspeed * Time.deltaTime);
			}
		} 

		if (buttonHeldRightP1)
		{	if (speed < maxspeed) {
				speed = speed * factor;
				player1.transform.RotateAround (new Vector3 (0, 0, 0), Vector3.forward, speed * Time.deltaTime);
			} else {
				player1.transform.RotateAround (new Vector3 (0, 0, 0), Vector3.forward, maxspeed * Time.deltaTime);
			}
		} 

		if (buttonHeldRightP2) {
			if (speed < maxspeed) {
				speed = speed * factor;
				player2.transform.RotateAround (new Vector3 (0, 0, 0), Vector3.forward, speed * Time.deltaTime);
			} else {
				player2.transform.RotateAround (new Vector3 (0, 0, 0), Vector3.forward, maxspeed * Time.deltaTime);
			}
		}
	}
}
