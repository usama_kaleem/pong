﻿using UnityEngine;
using System.Collections;

public class UIManager : MonoBehaviour 
{
	public GameObject gameOverMenu;
	public GameObject mainMenu;
	public GameObject hud;

	public static string[] comebacks = {"It's okay, second\nbest is good too",
										"My grandma can play\nbetter than that!",
										"You really need to\nwork on your game",
										"Don't start crying now",
										"I was born a winner",
		"Atleast you're\ngood at losing",
		"I can play this\nwith my eyes closed",
		"You sure you\nwanna do this?",
		"Not surprised at\nyour consistency",
		"Only if i had a buck\nfor everytime you lost..",
										"Wanna bet on this?",
										"HUSTLED",
		"THERE ARE LOSERS\nAND THEN THERE IS YOU",
		"CAN DO THIS\nALL DAY LONG",
										"BYE BYE FELICIA",
		"COME BACK WHEN\nYOU'RE READY CHILD",
		"YOU CANNOT BEAT\nTHE MASTER"};


	void Awake()
	{
		hud.SetActive(false);
		gameOverMenu.SetActive (false);
	}

	public void OpenMainMenu()
	{
		mainMenu.SetActive(true);
		gameOverMenu.SetActive (false);
	}

	public void OpenHUD()
	{
		hud.SetActive(true);
		mainMenu.SetActive (false);
		gameOverMenu.SetActive(false);

	}

	public void OpenGameOverMenu()
	{
		gameOverMenu.SetActive(true);
		hud.SetActive (false);
	}
}
