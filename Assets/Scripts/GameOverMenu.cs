﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class GameOverMenu : MonoBehaviour {
	
	public Ballmovement ballscript;
	public AIBallMovement aiballscript;
	public Text gamestats;
	public UIManager manager;

	bool hello = true;

	void Awake(){
		StartCoroutine(callblue ());
		StartCoroutine(callred ());
	}


	void Update(){

		if (hello&& Menu.mode==2) {
			
			if (ballscript.p1_score > ballscript.p2_score) {
				gamestats.text = "BLUE WINS";
				gamestats.color = new Color (0f, 209f, 238f, 0);
				StartCoroutine(callblue ());
			
				hello = false;

			} else {
				gamestats.text = "RED WINS";
				gamestats.color = new Color (1f, 0, 0, 0);
				StartCoroutine(callred ());

				hello = false;
				
			}

		}



		if (hello && Menu.mode==1) {

			if (ballscript.p1_score > ballscript.p2_score) {
				gamestats.text = "BLUE WINS";
				gamestats.color = new Color (0f, 209f, 238f, 0);
				StartCoroutine(callblue ());

				hello = false;

			} else {
				gamestats.text = "GREEN WINS";
				gamestats.fontSize = 90;
				gamestats.color = new Color (0,1, 0, 0);
				StartCoroutine(callgreen ());

				hello = false;

			}

		}

			
 
	}

	IEnumerator callblue(){

		ballscript.BlueRipple ();
		DOTween.To(() => gamestats.color, x => gamestats.color = x, new Color(0f, 209f, 238f ,255), 0.6f);
		yield return new WaitForSeconds(1.5f);
		hello = true;
	}


	IEnumerator callred(){

		ballscript.RedRipple ();
		DOTween.To(() => gamestats.color, x => gamestats.color = x, new Color(255, 0,0 ,255), 0.6f);
		yield return new WaitForSeconds(1.5f);
		hello = true;
	}


	IEnumerator callgreen(){

		aiballscript.GreenRipple ();
		DOTween.To(() => gamestats.color, x => gamestats.color = x, new Color(0, 255,0 ,255), 0.6f);
		yield return new WaitForSeconds(1.5f);
		hello = true;
	}

	// Use this for initialization
	public void ReplayClicked(){
		hello = true;
		if (Menu.mode == 1) {
			aiballscript.ReplayGame ();
		} else {
			ballscript.ReplayGame ();
		}
		manager.OpenHUD();


	}

	public void MainMenu(){

		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);

	}

	public void QuitGame(){
		Application.Quit();
	}
	
	// Update is called once per frame
}
