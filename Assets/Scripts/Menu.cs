﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class Menu : MonoBehaviour 
{
	public Playermovement playerscript;
	public UIManager manager;
	public AudioSource music;
	public GameObject ball, AIball, redpaddle, greenpaddle, startbutton1, startbutton2;
	public GameObject playicon, Logo, share, rate, volume, singleplayer, table, paddle;
	public GameObject redbuttons, bluebuttons, redscore, bluescore;
	public Transform player2Group;
	public static GameObject[] PedalControls, PedalControlsAI;
	public Text ready;
	public Image Arrow1, Arrow2;
	public static int mode = 0;


	void GetAllPedals(){

		PedalControls = GameObject.FindGameObjectsWithTag("controller");

		foreach (GameObject respawn in PedalControls) {
			respawn.SetActive (false);

		}

		PedalControlsAI = GameObject.FindGameObjectsWithTag("Aicontroller");

		foreach (GameObject respawn in PedalControlsAI) {
			respawn.SetActive (false);

		}

	}



	public void PlayClicked() 
	{	
		mode = 2;
		AIball.SetActive (false);
		greenpaddle.SetActive (false);
		startbutton2.SetActive (false);
		GetAllPedals ();
		StartCoroutine (Animate());
	}


	void DropMenuItems(){
		playicon.transform.DOMoveY(-40f, 0.8f);
		share.transform.DOMoveY(-40f, 0.8f);
		rate.transform.DOMoveY(-40f, 0.8f);
		volume.transform.DOMoveY(-40f, 0.8f);
		//singleplayer.transform.DOMoveY(-50f, 0.8f);
		Logo.transform.DOMoveY(650f, 0.8f);
		singleplayer.transform.DOMoveY(650f, 0.8f);
		paddle.transform.DOMoveY(-70f, 0.8f);
		table.transform.DOScale(0.722158f, 1f);
	}

	void BringOutArena(){

		Text score = redscore.GetComponent<Text> ();
		score.color = new Color (255f,0,0,255f);
		Arrow1.color = new Color (255f,0,0,255f);
		Arrow2.color =  new Color (255f,0,0,255f);

		Debug.Log ("refrewrt");

		player2Group.DOLocalMoveY(540, 0.8f);
//		redscore.transform.DOMoveY(450f, 0.8f);
		bluescore.transform.DOMoveY(50f, 0.8f);
//		redbuttons.transform.DOMoveY(420f, 0.8f);
		bluebuttons.transform.DOMoveY(70f, 0.8f);
		playerscript.player1.transform.DOMoveY(-2.4f, 0.8f);
		playerscript.player2.transform.DOMoveY(2.4f, 0.8f);

		
	}

	void BringOutSinglePLayerArena(){


		Text score = redscore.GetComponent<Text>();
		score.color = new Color (0f,255f,0f,255f);
		Arrow1.color = new Color (0f,255f,0f,255f);
		Arrow2.color =  new Color (0f,255f,0f,255f);



		//TODO: U dont need to move each gameobject individually. Just bundle them in a hierarchy and move the parent. 
		//And you need to watch the anchors tutorial again. I have fixed the issues but ull understand what u were missing if u just watch it. 
		//No need to write SetLoops(1, LoopType.Yoyo) at the end of every tween.
		//Use Localmove if u want to move the object in locally space. Move is used for worldspace.  

		player2Group.DOLocalMoveY(540, 0.8f);
//		redscore.transform.DOMoveY(450f, 0.8f);
		bluescore.transform.DOMoveY(50f, 0.8f);
//		redbuttons.transform.DOMoveY(420f, 0.8f);
		bluebuttons.transform.DOMoveY(70f, 0.8f);
		playerscript.player1.transform.DOMoveY(-2.4f, 0.8f);
		playerscript.player3.transform.DOMoveY(2.4f, 0.8f);


	}


	public void ArenaFall(){

		player2Group.DOLocalMoveY(540, 0.8f);
//		redscore.transform.DOMoveY(609f, 0.8f);
		bluescore.transform.DOMoveY(-97f, 0.8f);
//		redbuttons.transform.DOMoveY(560.6f, 0.8f);
		bluebuttons.transform.DOMoveY(-63.3f, 0.8f);
		playerscript.player1.transform.DOMoveY(-6.39f, 0.8f);
		playerscript.player2.transform.DOMoveY(5.688f, 0.8f);

	}


	IEnumerator Animate(){
		DropMenuItems ();
		yield return new WaitForSeconds (1f);
		BringOutArena ();
		yield return new WaitForSeconds (0.8f);
		ready.text = "TAP TO DUEL";
		manager.OpenHUD ();
	}

	IEnumerator SinglePlayerAnimate(){
		DropMenuItems ();
		yield return new WaitForSeconds (1f);
		BringOutSinglePLayerArena ();
		yield return new WaitForSeconds (0.8f);
		ready.text = "TAP TO DUEL";
		manager.OpenHUD ();
	}


	public void VolumeClicked() 
	{
		if (music.mute == false) {
			music.mute = true;
		} else {
			music.mute = false; 

		}
	}

	public void RateUsClicked()
	{
		new RateUsController().Rate();
	}

	public void AIClicked()
	{
		mode = 1;
		ball.SetActive (false);
		redpaddle.SetActive (false);
		redbuttons.SetActive(false);
		startbutton1.SetActive (false);
		GetAllPedals ();
		StartCoroutine (SinglePlayerAnimate());
	}
	

}
