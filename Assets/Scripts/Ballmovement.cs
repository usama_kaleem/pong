﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//using Holoville.HOTween;
using DG.Tweening;

public class Ballmovement : MonoBehaviour {

	public Rigidbody2D ball;
	public GameObject ballgo;
	public GameObject table, red, blue;
	public Menu menu;
	public Text p1text, p2text, p1taunt, p2taunt, p1points, p2points, ready;
	public Playermovement playerscript;
	//public PedalMovement playerscriptL1,playerscriptL2,playerscriptR1,playerscriptR2;
	//public SpriteRenderer tableclr;
	public GameObject sparks;
	public GameObject bluesparks;
	public AudioClip sound;
	public int p1_score, p2_score = 0;
	public AudioSource audioSource;
	public GameObject tap_start; // for gameplay
	public UIManager manager;
	public float dev = 0.2f; //increase this to deviate more
	public GameObject redripple;
	public GameObject blueripple;
	public GameObject p1left, p1right, p2left, p2right;
	public float delta = 0.4f;
	public GameObject explosion;
		
	private int gamesetp1 = 0;
	private int gamesetp2 = 0;
	private int num_of_games = 5;
	private Transform p1_trans, p2_trans;
	private Collider2D p1_collider, p2_collider;
	private float speed = 3.5f;
	private float originalspeed = 3.5f;
	private float maxspeed = 10f;
	private bool turn_bool = false;
	private int count_of_games = 1;
	private SpriteRenderer tablesprite, bluepadSprite, redpadSprite;
	private Sprite redTable, blueTable;


	void Awake() {
		bluepadSprite = blue.GetComponent<SpriteRenderer> ();
		redpadSprite = red.GetComponent<SpriteRenderer> ();
		tablesprite = table.GetComponent<SpriteRenderer> ();
	//	HitTable ();
	//	RedRipple ();
		redTable=  Resources.Load<Sprite>("arenared");
		blueTable =  Resources.Load<Sprite>("arenablue");


		//	HOTween.To (ready.rectTransform, 0.4f, new TweenParms ().Prop("localScale", new Vector3(1.25f, 1.25f, 0f)).Loops (-1, LoopType.Yoyo));
		ready.rectTransform.DOScale(new Vector3(1.25f, 1.25f, 0f), 0.4f).SetLoops(-1, LoopType.Yoyo);

		display_score ();
		p1_trans = playerscript.player1.transform;
		p2_trans = playerscript.player2.transform;
		p1_collider = playerscript.player1.GetComponent<EdgeCollider2D> ();
		p2_collider = playerscript.player2.GetComponent<EdgeCollider2D> ();
		Random.seed = System.DateTime.Now.Millisecond;



	}



	public void TapClicked(){
		p1taunt.color = new Color (255f, 0f, 0f, 255);
		//HOTween.Kill (ready.rectTransform);
		tap_start.SetActive (false);
		foreach (GameObject respawn in Menu.PedalControls) {
			respawn.SetActive (true);
		}
		ready.text = " ";
		ResetBall ();
		reset_Pedals ();

	}

	IEnumerator Taunting(bool turn){

		int rand = Random.Range (1,16);


		if (turn == false) {
			p1taunt.text = UIManager.comebacks[rand];

			p1taunt.rectTransform.localScale = Vector3.zero;
			//	HOTween.To (p1taunt.rectTransform, 0.8f, new TweenParms ().Prop("localScale", new Vector3(1.5f, 1.5f, 0f)).Loops (1, LoopType.Yoyo));
			p1taunt.rectTransform.DOScale(new Vector3(1.5f, 1.5f, 0f), 1f).SetEase(Ease.OutElastic);


		} else {

			p2taunt.text = UIManager.comebacks[rand];
			p2taunt.rectTransform.localScale = Vector3.zero;
			//	HOTween.To (p2taunt.rectTransform, 0.8f, new TweenParms ().Prop("localScale", new Vector3(-1.25f, -1.25f, 0f)).Loops (1, LoopType.Yoyo));
			p2taunt.rectTransform.DOScale(new Vector3(-1.25f, -1.25f, 0f), 1f).SetEase(Ease.OutElastic);



		}


		yield return new WaitForSeconds(2f);

		p2taunt.text = "";
		p1taunt.text = "";


	}


	IEnumerator invisible(){

		yield return new WaitForSeconds(2.5f);
		if (p1_score == num_of_games || p2_score == num_of_games) {
			if (p1_score > p2_score) {
				reset_Pedals ();
				tablesprite.sprite = blueTable;
			} else {
				reset_Pedals ();
				tablesprite.sprite = redTable;
			}
			ReplayCanvas();

		} else {

			ResetGame ();
		}


	}


	void OnBecameInvisible()
	{	
		StartCoroutine(invisible ());

	}




	void OnTriggerEnter2D(Collider2D col){

		if (col.gameObject.CompareTag ("Player")) {

			audioSource.PlayOneShot (sound, 5f);
			flip ();
			HitTable ();

			if (speed <= maxspeed) {
				speed += delta;
			}


		} else {
			GameObject explode = Instantiate (explosion, ball.transform.position , Quaternion.identity) as GameObject;
			Destroy (explode, 1f);
			Vector3 temp = new Vector3 (0, 8.1f, -3.3f);
			ball.transform.position = temp;
			ball.velocity = Vector2.zero;


			if (turn_bool == false) {
				p2_score++;
				StartCoroutine (Taunting(turn_bool));


			} else if (turn_bool == true) {
				p1_score++;
				StartCoroutine (Taunting(turn_bool));
			}



			display_score ();


		}

	}


	public void RedRipple(){

		GameObject ripple = Instantiate (redripple, Vector3.zero , Quaternion.identity) as GameObject;

		DOTween.Kill(ripple.transform);
		DOTween.To(() => ripple.transform.localScale, x => ripple.transform.localScale = x, new Vector3 (2.5f, 2.5f, 0f), 0.7f).SetLoops(2,LoopType.Yoyo);


		SpriteRenderer temp = ripple.GetComponent<SpriteRenderer>();
		DOTween.To(() => temp.color, x => temp.color = x, new Color(255, 255, 255 ,0), 0.9f);
		Destroy (ripple, 1.1f);




	}


	public void BlueRipple(){


		GameObject ripple = Instantiate (blueripple, Vector3.zero , Quaternion.identity) as GameObject;

		DOTween.Kill(ripple.transform);
		DOTween.To(() =>ripple.transform.localScale, x => ripple.transform.localScale = x, new Vector3 (2.5f, 2.5f, 0f), 0.3f).SetLoops(2,LoopType.Yoyo);

		SpriteRenderer temp = ripple.GetComponent<SpriteRenderer>();
		DOTween.To(() => temp.color, x => temp.color = x, new Color(255, 255, 255 ,0), 0.5f);
		Destroy (ripple, 0.8f);
	}


	void HitTable(){

		DOTween.Kill(table.transform);

		DOTween.To(() => table.transform.localScale, x => table.transform.localScale = x, new Vector3(0.7f, 0.7f, 0), 0.1f).SetLoops(2,LoopType.Yoyo);
	}


	void flip(){
		//player 2 turn


		if (turn_bool == true) {

			Vector2 pedalPos = new Vector2(p2_trans.transform.position.x, p2_trans.transform.position.y);
			Vector2 left = new Vector2 (p2left.transform.position.x, p2left.transform.position.y);
			Vector2 right = new Vector2 (p2right.transform.position.x, p2left.transform.position.y);


			if (PedalMovement.buttonHeldLeftP2 == true) {
				left.Normalize ();
				ball.velocity = speed * -1f * left;
			} else if (PedalMovement.buttonHeldRightP2 == true) {

				right.Normalize ();
				ball.velocity = speed * -1f * right;

			} else {
				pedalPos.Normalize ();
				ball.velocity = speed * -1 * pedalPos;

			}



			RedRipple ();
			tablesprite.sprite = blueTable;

			GameObject sparkobj2= Instantiate (sparks, ball.transform.position ,p1_trans.rotation) as GameObject;
			Destroy (sparkobj2, 0.5f);

			turn_bool = false;

			Color tmp = bluepadSprite.color;
			tmp.a = 1f;
			bluepadSprite.color = tmp;

			tmp = redpadSprite.color;
			tmp.a = 0.5f;
			redpadSprite.color = tmp;


			delay1();



		}
		//player 1 turn
		else {

			Vector2 pedalPos = new Vector2(p1_trans.transform.position.x, p1_trans.transform.position.y);
			Vector2 right = new Vector2(p1right.transform.position.x, p1right.transform.position.y);
			Vector2 left = new Vector2(p1left.transform.position.x, p1left.transform.position.y);


			if (PedalMovement.buttonHeldLeftP1 == true) {
				(left).Normalize ();
				ball.velocity = speed * -1 * left;
			} else if (PedalMovement.buttonHeldRightP1 == true) {
				(right).Normalize ();
				ball.velocity = speed * -1 * right;


			} else {
				(pedalPos).Normalize ();
				ball.velocity = speed * -1 * pedalPos;

			}


			BlueRipple ();
			tablesprite.sprite = redTable; 


			GameObject sparkobj= Instantiate (bluesparks, ball.transform.position ,p1_trans.rotation) as GameObject;
			Destroy (sparkobj, 0.5f);


			turn_bool = true;

			Color tmp = bluepadSprite.color;
			tmp.a = 0.5f;
			bluepadSprite.color = tmp;

			tmp = redpadSprite.color;
			tmp.a = 1f;
			redpadSprite.color = tmp;

			delay2();

		}

	}


	void ResetGame(){
		display_score ();
		reset_Pedals ();
		ball.velocity = Vector2.zero;
		StartCoroutine (readysetgo());
		count_of_games++;
		//HOTween.Kill (p1taunt.rectTransform);
		//HOTween.Kill (p2taunt.rectTransform);

	}



	void ReplayCanvas (){

		PedalMovement.buttonHeldLeftP1 = false;
		PedalMovement.buttonHeldRightP1 = false;
		PedalMovement.buttonHeldLeftP2 = false;
		PedalMovement.buttonHeldRightP2 = false;

		if (p1_score > p2_score) {
			gamesetp1++;


			//winnersArena.sprite = blueTable;
		} else {
			gamesetp2++;


			//winnersArena.sprite = redTable;
		}
		menu.ArenaFall ();
		p1points.text = "" + gamesetp1;
		p2points.text = "" + gamesetp2;
		manager.OpenGameOverMenu ();
	}

	public void ReplayGame (){
		p2points.text = "";
		p1points.text = "";
		p1_score = 0;
		p2_score = 0;
		display_score ();
		count_of_games = 1;
		num_of_games = 5;
		reset_Pedals();
		ball.velocity = Vector2.zero;
		ball.transform.position = new Vector3 (0,0,-3.3f);
		tap_start.SetActive (true);
		ready.text = "Tap to Start";




	}


	void ResetBall(){

		ball.transform.position = new Vector3 (0,0,-3.3f);
		speed = originalspeed;
		int rand = Random.Range(0,2);
		if (rand == 0) {
			ball.velocity = Vector2.up * speed;
			tablesprite.sprite = redTable;
			turn_bool = true;
		} else {
			ball.velocity = Vector2.down * speed;
			tablesprite.sprite = blueTable;
			turn_bool = false;

		}

	}

	void Reset_positions(){

		ResetBall ();
		reset_Pedals ();


	}

	void reset_Pedals(){

		Quaternion target = Quaternion.Euler(0, 0, -90f);
		p1_trans.position = new Vector3 (0,-2.4f, 0);
		p2_trans.position = new Vector3(0, 2.4f, 0);
		p1_trans.rotation = target;
		p2_trans.rotation = target;
		p1_collider.enabled = true;
		p2_collider.enabled = true;
		Color tmp = bluepadSprite.color;
		tmp.a = 1f;
		bluepadSprite.color = tmp;

		tmp = redpadSprite.color;
		tmp.a = 1f;
		redpadSprite.color = tmp;
	}

	void display_score(){

		p1text.text = "" + p1_score;
		p2text.text = "" + p2_score;

	}




	void delay1() {
		p1_collider.enabled = true;

		p2_collider.enabled = false;

	}

	void delay2() {
		p1_collider.enabled = false;

		p2_collider.enabled = true;

	}

	IEnumerator readysetgo() {

		ready.text = "READY!";
		//	HOTween.To (ready, 0.2f, new TweenParms ().Prop("color", new Color(255, 255, 255, 0)).Loops (2, LoopType.Yoyo));
		yield return new WaitForSeconds(0.5f);
		ready.text = "GO!";
		yield return new WaitForSeconds(0.5f);
		ready.text = "";
		Reset_positions();

	}

}